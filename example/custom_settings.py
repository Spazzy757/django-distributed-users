import os

USER_DB = 'distributed_user_db'
DATABASE_ROUTERS = ['distributed_user.db.UserRouter']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('DATABASE_NAME', 'postgres'),
        'USER': os.environ.get('DATABASE_USER', 'postgres'),
        'HOST': os.environ.get('DATABASE_HOST', 'db'),
        'PORT': os.environ.get('POSTGRES_DB_PORT', '5432'),
        'PASSWORD': None,
    },
    'distributed_user_db': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('DATABASE_NAME', 'postgres'),
        'USER': os.environ.get('DATABASE_USER', 'postgres'),
        'HOST': os.environ.get('DATABASE_HOST', 'user_db'),
        'PORT': os.environ.get('POSTGRES_DB_PORT', '5432'),
        'PASSWORD': None,
    }
}
