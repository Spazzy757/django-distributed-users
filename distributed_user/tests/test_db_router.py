# -*- coding: utf-8 -*-

from django.contrib.auth import get_user_model
from example.models import Example
from django.test import TestCase
from faker import Faker


class AuthUserRouterTestCase(TestCase):
    multi_db = True

    def setUp(self):
        f = Faker()
        get_user_model().objects.create(username=f.first_name())
        Example.objects.create(name='Test')

    def test_user_gets_created_in_user_db(self):
        self.assertEqual(
            get_user_model().objects.using('distributed_user_db').count(),
            1
        )

    def test_user_not_in_default_db(self):
        self.assertEqual(
            get_user_model().objects.using('default').count(),
            0
        )

    def test_example_object_in_default_db(self):
        self.assertEqual(
            Example.objects.count(),
            1
        )

    def test_example_object_not_in_user_db(self):
        self.assertEqual(
            Example.objects.using('distributed_user_db').count(),
            0
        )

