from django.apps import AppConfig


class DistributedUserConfig(AppConfig):
    name = 'distributed_user'
